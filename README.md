
# Table of Contents

1.  [TP2](#orga52fd06)
2.  [TP2b](#org7a1d400)
    1.  [adivinador.js](#org8b64280)
3.  [Licencia](#org7f0141a)

Sistemas Inteligentes - Año 2022


<a id="orga52fd06"></a>

# TP2

El Trabajo Práctico N° 2 implementa dos tipos de algoritmos de búsqueda: El Hill Climbing y el Simmulated Annealing.

Se propone un ejercicio simple: implementar f(x) = x<sup>2</sup>, y considerando un x inicial, optimizar la función hacia el menor valor posible.

Hay una implementación en Racket Scheme con interfaz gráfica. También, puede cargarse la librería sobre el REPL de Racket para poder tener mayor control del algoritmo. Para utilizar la interfáz gráfica, no se requiere instalar bibliotecas o paquetes externos.

GIFs mostrando cómo es el Hill Climbing y el Simmulated Annealing implementado en Racket:

![img](./imgs/tp2-racket-hill-climbing.gif)

![img](./imgs/tp2-racket-simmulated-annealing.gif)


<a id="org7a1d400"></a>

# TP2b


<a id="org8b64280"></a>

## adivinador.js

Es una adaptación del código escrito para la Web por Matías Ortuño ([ver la página Web](https://github.com/cnngimenez/matiasortuno-IAJuegos/tree/master/TP1)). Se provee `prueba-adivinador.rb` que es una implementación de una solución de fuerza bruta.


<a id="org7f0141a"></a>

# Licencia

Este trabajo se encuentra bajo la licencia GNU General Public License version 3 (GPLv3).
Christian Gimenez - 2022.

