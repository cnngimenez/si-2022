#lang racket/gui

;; Copyright 2022 Christian Gimenez
;;
;; hill-climbing-gui.rkt
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require "hill-climbing.rkt")
(require "ej3.rkt")

(define the-problem (ej3-create-problem -10))
(define the-hc-state (hill-climbing-start the-problem))

(define (update-interface hc-prev hc-current)
  (send lbl-hc-prev-state set-label (format "Ejecución previa: ~a" hc-prev))
  (send lbl-hc-current-state set-label (format "Ejecución actual: ~a" hc-current))
  (if (hc-state-return hc-current)
      (send lbl-results set-label (format "Resultado: ~a"
                                          (hc-state-return hc-current)))
      (send lbl-results set-label "Sin resultado aún.")))

(define frame (new frame% [label "Hill Climbing"]))

(define msg
  (new message% [parent frame] [label "Haga clic en aceptar."]))

(define txt
  (new text-field% [parent frame] [init-value "-10"] [label "Valor inicial:"]))

(new button% [parent frame]
     [label "Reiniciar"]
     [callback (lambda (button event)
                 (set! the-problem (ej3-create-problem
                                    (string->number (send txt get-value))))
                 (set! the-hc-state (hill-climbing-start the-problem))
                 (update-interface the-hc-state the-hc-state))])

(define result-pane (new vertical-panel% [parent frame]))

(new message% [parent result-pane] [label "Léase: #<hc-state nodo-actual, nodo-vecino, retorna?>
        #<node estado f(estado) profundidad>"])

(define lbl-hc-prev-state
  (new message% [parent result-pane] [label "Estado previo del Hill Climbing"]
       [auto-resize #t]))

(define lbl-hc-current-state
  (new message% [parent result-pane] [label "Estado actual del Hill Climbing"]
       [auto-resize #t]))

(define lbl-results
  (new message% [parent result-pane] [label "Sin resultado aún."]
       [auto-resize #t]))

(new button% [parent frame]
     [label "Paso >>"]
     [callback (lambda (button event)
                 (let ([result (hill-climbing-step the-problem the-hc-state)])
                   (update-interface (first result) (second result))
                   (set! the-hc-state (second result))))])

(send frame show #t)
