Implementación del ejercicio 3 del TP2 en Racket Scheme.

# Ejercicio 3 - Trabajo Práctico 2 de Sistemas Inteligentes

> Ejercicio 3: Considerar el problema de optimización f (x) = x2 , x?[−2, 2].
>
> (a) Aplicar hill climbing para encontrar el mı́nimo de la función f .
>
> (b) Aplicar simulated annealing para encontrar el mı́nimo de la función f .

# Uso
Instalar racket en el sistema y ejecutar en una terminal uno de los siguientes comandos:

```
racket hill-climbing-gui.rkt
racket simmulated-annealing-gui.rkt
```


# Licencia
Este trabajo se encuentra bajo la licencia GNU General Public License version 3 (GPLv3).
Christian Gimenez - 2022
