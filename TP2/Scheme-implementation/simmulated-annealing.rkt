#lang racket

;; Copyright 2022 Christian Gimenez
;;
;; simmulated-annealing.rkt
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(provide simmulated-annealing
         simmulated-annealing-with-debug
         simmulated-annealing-start
         simmulated-annealing-step
         (struct-out sa-state))

(require "problem.rkt")

(define (pick-random lst)
  (list-ref lst
            (random 0 (length lst) )))

(define (random-successor node the-problem)
  (let* ([successors (problem-successors-function the-problem)])
    (pick-random (successors node))))

(define (probability delta temp)
  (if (not (= temp 0))
      (exp (/ delta temp))
      0))

(define (probability? delta temp)
  (< (random) (probability delta temp)))

(define (simmulated-info T t ΔE [chosen null])
  (remove null (list
                (list 'temp T)
                (list 'step t)
                (list 'delta ΔE)
                (list 'prob (probability ΔE T))
                (if (null? chosen)
                    null
                    (list 'chosen chosen)))))

(define (simmulated-annealing-with-debug
         the-problem the-schedule
         [step-function (lambda (current neighbour simmulation-choices)
                          null)])
  (let* ([create-node (problem-create-node-function the-problem)]
         [maximum-optimisation? (equal? (problem-optimisation-type the-problem)
                                   'maximum-optimisation)]
         
         [current (create-node (problem-initial-state the-problem))]
         [next null]
         [T 0]
         [return #f]
         [ΔE 0]

         ;; To show on the step-function
         [current-debug null]
         [next-debug null]
         [chosen-debug #f])
    
    (do ((t 1 (+ t 1))) (return)
      (set! T (the-schedule t))
      (when (< T 0.01)
        (set! return current))

      (set! next (random-successor current the-problem))

      (if maximum-optimisation?
          (set! ΔE (- (node-value next) (node-value current)))
          (set! ΔE (- (node-value current) (node-value next))))

      ;; For the step-function
      (set! next-debug next)
      (set! current-debug current)
      (set! chosen-debug #f)
      ;; -----
            
      (if (> ΔE 0)
          (begin (set! current next)
                 (set! chosen-debug #t))
          (when (probability? ΔE T)
            (set! current next)
            (set! chosen-debug #t)))

      ;; Debug: Show the step
      (step-function current-debug next-debug T t ΔE chosen-debug))

    return))

(define (simmulated-annealing the-problem the-schedule
                              [step-function (lambda (current neighbour simmulation-choices)
                                               null)])
  (let* ([create-node (problem-create-node-function the-problem)]
         [maximum-optimisation? (equal? (problem-optimisation-type the-problem)
                                        'maximum-optimisation)]
         
         [current (create-node (problem-initial-state the-problem))]
         [next null]
         [T 0]
         [return #f]
         [ΔE 0])
    
    (do ((t 1 (+ t 1))) (return)
      (set! T (the-schedule t))
      (when (< T 0.01)
        (set! return current))
      
      (set! next (random-successor current the-problem))

      (if maximum-optimisation?
          (set! ΔE (- (node-value next) (node-value current)))
          (set! ΔE (- (node-value current) (node-value next))))

      (step-function current next (simmulated-info T t ΔE))
      
      (if (> ΔE 0)
          (set! current next)
          (when (probability? ΔE T)            
            (set! current next))))
      


    return))

(struct sa-state (current next t T ΔE chosen return)
  #:methods gen:custom-write
  [(define (write-proc sa-state port mode)
     (fprintf port "#<sa-state ~a next=~a t=~a T=~a ΔE=~a Prob=~a chosen=~a
           return=~a>"
              (sa-state-current sa-state)
              (sa-state-next sa-state)
              (sa-state-t sa-state)
              (sa-state-T sa-state)
              (sa-state-ΔE sa-state)
              (probability (sa-state-ΔE sa-state) (sa-state-T sa-state))
              (sa-state-chosen sa-state)
              (sa-state-return sa-state)))])


(define (simmulated-annealing-start the-problem the-schedule)
  (let ([create-node (problem-create-node-function the-problem)])
    (sa-state (create-node (problem-initial-state the-problem))
              (create-node (problem-initial-state the-problem))
              1 ;; t
              0 ;; T
              0 ;; ΔE
              #f ;; chosen
              ;; return:
              #f)))

(define (simmulated-annealing-step the-problem the-schedule the-sa-state)
  (let* ([maximum-optimisation? (equal? (problem-optimisation-type the-problem)
                                        'maximum-optimisation)]
         
         [current (sa-state-current the-sa-state)]
         [next (sa-state-next the-sa-state)]
         [T (sa-state-T the-sa-state)]
         [return (sa-state-return the-sa-state)]
         [ΔE (sa-state-ΔE the-sa-state)]
         [t (sa-state-t the-sa-state)]
         [chosen #f])

    (unless return
      (set! T (the-schedule t))
      (when (< T 0.01)
        (set! return current))
      
      (set! next (random-successor current the-problem))

      (if maximum-optimisation?
          (set! ΔE (- (node-value next) (node-value current)))
          (set! ΔE (- (node-value current) (node-value next))))
      
      (if (> ΔE 0)
          (begin 
            (set! current next)
            (set! chosen #t))
          (when (probability? ΔE T)            
            (set! current next)
            (set! chosen #t)))
      
      (set! t (+ t 1)))

    ;; Return a list with previous and current state.
    (list the-sa-state
          (sa-state current next t T ΔE chosen return))))
