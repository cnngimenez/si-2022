#lang scribble/manual

@require[@for-label[problem
                    racket/base]]

@title{Modulo problem}
@author{cnngimenez}

@defmodule{problem}

@[table-of-contents]

@section{Estructuras}

@defstruct[node ([state any?] [value number?] [depth number?])]{
Un nodo del árbol de búsqueda.

El estado @racket{state} puede ser cualquier tipo: un número o una estructura de dato. El valor @racket{value} es el resultado de la función de evaluación del problema (f) aplicado al estado ( @racket{(f state)} ). Finalmente, @racket{depth} es la profundida del nodo en el árbol de búsqueda.
}

@defstruct[problem (
[initial-state any?]
[f procedure?]
[successors-function procedure?]
[highest-successor-function procedure?]
[create-node-function procedure?]
[optimisation-type symbol?]
[step-function procedure?]){
Definición de un problema. Una instancia de esta estructura es un problema listo para ser utilizado por los programas en Racket.

El campo @racket{initial-state} contiene el estado inicial (no el valor). La función de evaluación @racket{f} está definida como @emph{f(estado) = valor}. Luego, las funciones son:

@itemlist[
  @item{@racket{successors-function}
  Una función @emph{successors : nodo \to P(nodo)} que retorna los sucesores de un nodo.
  
  @racket{(define (successors node) ...)}}
  
  @item{@racket{highest-successor-function}
  Una función @emph{highest-succesor : nodo \to nodo} que retorna el sucesor más óptimo de un nodo.
  
  @racket{(define (highest-successor node) ... )}}
  
  @item{@racket{create-node-function}
  Una función que crea un nuevo nodo a partir de un estado y, opcionalmente, el nodo padre.
  
  @racket{(define (create-node state [parent null]) ... )}}
  
  @item{@racket{optimisation-type}
  Uno de los siguientes símbolos: @racket{'minimum-optimisation} o @racket{'maximum-optimisation}.}

  @item{@racket{step-function}
  Función que es llamada por cada paso del algoritmo. El objetivo es mostrar los distintos valores que el algoritmo va utilizando. Recibe dos parámetros obligatorios y 

@racket{(define (ej3-print-step current neighbour
                        [temp null] [step null] [delta null]
                        [prob null] [chosen null]) ... )}}
  
]
}
