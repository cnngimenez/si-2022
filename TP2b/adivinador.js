#!/usr/bin/env node

/**
 Este código es adaptado de [1] para que funcione en la terminal y no en un explorador Web. El objetivo es que este código fuente puda usarse para implementar una solución con Algoritmos Genéticos.

 El código original es de Matías Ortuno y Pablo Kogan. El mismo se puede descargar desde [2].

 Para utilizarse: cambiar `myprogram_command` y `myprogram_args` por el nombre del programa y los parámetros que recibe. Por
 ejemplo: si en la terminal se utiliza "bash mi_programa --no-repetir --algoritmo-num 20" utilizar lo siguiente
 ```
 const myprogram_command = 'bash';
 const myprogram_args = ['mi_programa', '--no-repetir', '--algoritmo-num 20'];
 ```
 
 [1] https://github.com/cnngimenez/matiasortuno-IAJuegos/tree/master/TP1
 [2] https://github.com/matiasortuno/matiasortuno.github.io
*/

const myprogram_command = 'ruby';
const myprogram_args = ['prueba-adivinador.rb'];
// const myprogram_command = 'fish';
// const myprogram_args = ['read.fish'];

// Función que genera y retorna un número aleatorio de 4 cifras
function generarAleatorio() {
    var x = parseInt((Math.random() * 8999) + 1000);
    return x;
}

// Función que llama a realizar todas las verificaciones particulares entre nros
function verificar(n2, estado) {
    var band=false;
    var bien = 0;
    var regular = 0;
    var mal = 0;
       
    //Separo los digitos (primero de los adivinados y luego del aleatorio,
    // y los almaceno en su respectivo arreglo
    var nAdivinados= separarDigitos(n2);
    //    console.log(nAdivinados);
    //Guardo el valor de el nro adivinado para mostrarlo antes de que se modifique
    var num = estado.secreto;
    var nAleatorios=separarDigitos(num);
    //    console.log(nAleatorios);

    //verifico cuantos números están "bien"
    var res = verificarBien(nAdivinados, nAleatorios);
    bien = res[0].length;

    nAdivinados = res[1];
    
    //verifico cuántos números están "regulares"
    var res2 = verificarRegulares(nAdivinados, nAleatorios);
    regular = res2[0].length;

    //Calculo cuántos números están mal
    //    var mal = 4 - (res[0].length + res2[0].length);
    var mal = 4 - (bien + regular);
    
    //Si encontré que los 4 dígitos están bien, entonces corto
    if(bien==4){
        band=true;
    }
    
    return {
        entrada: n2,
        secreto: estado.secreto,
        bien: bien,
        regular: regular,
        mal: mal,
        adivinado: band,
        contador: estado.contador + 1
    }
}

//Función en la que ingresa un string con los 4 digitos de un número y 
//los separa almacenando cada uno en una posición distinta del arreglo, 
//retorna ese arreglo
function separarDigitos(n){
    var dad1 = parseInt(n % 10);
    var dad2 = parseInt((n % 100) / 10);
    var dad3 = parseInt((n / 100) % 10);
    var dad4 = parseInt(n / 1000);
    var nSeparados = [dad4, dad3, dad2, dad1];
    return nSeparados;
}

//Función que verifica cuántos dígitos del número a adivinar están bien, y los
//elimina del arreglo (nAdi) para verificar sólo los restantes luego, también
//retorna los dígitos que están bien en un arreglo nuevo (nBien).
function verificarBien(nAd, nAl) {
    var nBien = [];
    var nAdi = [], nAle = [];
    var resultados = [nBien, nAdi];
    var j = 0, k = 0;
    for (var i = 0; i < 4; i++) {
        if (nAd[i] == nAl[i]) {
            nBien[j] = nAd[i];
            j++;
        }
        else {
            nAdi[k] = nAd[i];
            k++;
        }
    }
    return resultados;
}

//Función que verifica cuántos números son regulares
function verificarRegulares(nAd, nAl) {
    var band=false;
    var nReg = [];
    var nAdi = [], nAle = [];
    var resultados = [nReg, nAd, nAl];
    var aux, k = 0;
    var longAd = nAd.length, longAl = nAl.length;
    for (var i = 0; i < longAd; i++) {
        for (var j = 0; j < longAl; j++) {
            if(band==false){
                if (nAd[i] == nAl[j]) {
                    band=true;
                    aux = nAd[i];
                    nReg[k] = aux;
                    k++;
                }
            }
        }
        band=false;
    }
    return resultados;
}

/**
 Imprimir el estado para que el usuario sepa si ganó o no.
 */
function generar_resultado(estado) {
    if (estado.adivinado) {
        return '¡Adivinaste!\n';
    }

    return `bien: ${estado.bien};regular: ${estado.regular}; mal: ${estado.mal}; contador: ${estado.contador}\n`
}

/**
 Main loop
*/
function repl_loop() {
    var estado = {
        entrada: null,
        secreto: generarAleatorio(),
        bien: 0,
        regular: 0,
        mal: 0,
        adivinado: false,
        contador: 0
    }
    var entrada = null;
    var salida = null;

    const { spawn } = require('node:child_process');    
    const myprogram = spawn(myprogram_command, myprogram_args);

    console.log(`Program started: ${myprogram_command} ${myprogram_args}`);
      
    myprogram.stdout.on('data', (entrada) => {
        if (estado.adivinado) {
            /* Pueden ejecutarse varios eventos al mismo tiempo.
              Esto intenta evitar que se muestren más resultados una vez adivinado el número. */
            return;
        }
        
        console.log(`Número secreto: ${estado.secreto}`);
        console.log(`< Respuesta: ${entrada}`);
        estado = verificar(entrada, estado);        
        salida = generar_resultado(estado);
        myprogram.stdin.write(salida);
        console.log(`> Enviado: ${salida}`);

        if (estado.adivinado) {
            myprogram.stdin.write('¡Adivinaste!');
            myprogram.kill();
        }
    });
    
    myprogram.on('close', (code) => {
        if (code != null) {
            /* Si Code == null, entonces el programa se cerró por kill()
              (myprogram no contiene un proceso activo). */
            console.log(`Program ended unexpectedly. Return code: ${code}`);
        }
    });

    salida = '¡Hola!\n';
    if (!myprogram.stdin.write(salida)) {
        console.warn(`Error al enviar ${salida}`);        
    }
    console.log(`> Sent: ${salida}`);
}


repl_loop();
