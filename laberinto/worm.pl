%% worm.pl
%% Author: Gimenez, Christian.
%%
%% Copyright (C) 2024 Gimenez, Christian
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%
%% april 2024

:- module(worm, [
              create_worm_passage/2,
              create_worm_passage2/2
          ]).
/** <module> name: 

@author Christian Gimenez
@license GPLv3
*/

:- use_module(labyrinth2D).

ponderate_directions(up, 0.225).
ponderate_directions(down, 0.225).
ponderate_directions(left, 0.05).
ponderate_directions(right, 0.50).

generate_intervals(Lst, Intervals) :-
    %% TODO: Generate intervals for less options in Lst (for a list of 3 or 2 elements instead of 4).
    generate_intervals_int(0, Lst, Intervals).

generate_intervals_int(1.0, [], []) :- !.
generate_intervals_int(Num, [Elt|Rest], [Elt-Num-Max|IntRest]) :-
    ponderate_directions(Elt, Delta), !,
    Max is Num + Delta,
    generate_intervals_int(Max, Rest, IntRest).

direction_num(Num, Element, Intervals) :-
    member(Element-Min-Max, Intervals),
    Num >= Min,
    Num < Max.

random_direction(Direction) :-
    random_direction([up, down, left, right], Direction).

random_direction(Lst, Element) :-
    random(Num),
    generate_intervals(Lst, Intervals),
    direction_num(Num, Element, Intervals).

%! random_step(+Size: term, +Pos: term, -NewPos: term) is det.
%
% Select a new position fro Pos that do not exceeds the limits of the map size.
%
% @param Size The map size. A size/2 term with Width and Height.
% @param Pos The starting position. A pos/2 term with X, Y coordinates.
% @param NewPos The next position selected randomly. A pos/2 term with X, Y coordinates.
random_step(Size, Pos, NewPos) :-
    random_step(Size, Pos, NewPos, [up, down, left, right]).

random_step(size(Width, Height), pos(X, Y), pos(X2, Y2), LstDirections) :-
    %% If calc_step/4 fails, no backtracking will be done, and random_member/2 will
    %% not select another direction. Thus repeat/0 is used to make the backtracking
    %% happen.
    repeat,    
    random_direction(LstDirections, Direction),
    %% TODO: What happens if randomly it selects the same direction many times!? Remove direction after backtracking!
    %% TODO: Select directions with specific weight probability. For instance, select right with 35% probability and left with 15%.
    calc_step(size(Width, Height), pos(X, Y), Direction, pos(X2, Y2)), !.

%! worm_step(+Map: list, +Size: number, +Pos: number, -NewPos: term, -NewMap: list)
%
% Make a worm setp.
%
% A "worm step" next step is a position up, down, left, right that is not empty nor outside the map.
% An empty position means that the worm is comming back to its track, or its on another worm.
worm_step(Map, size(Width, Height), pos(X, Y), pos(X2, Y2), NewMap) :-
    map_find_around(Map, pos(X,Y), empty, LstDirections),
    random_step(size(Width, Height), pos(X, Y), pos(X2, Y2), LstDirections),
    map_insert(Map, pos(X2, Y2), empty, NewMap).
worm_step(Map, _Size, Pos, Pos, Map). %% Cannot make any new step...

worm_step_ignore_empty(Map, size(Width, Height), pos(X, Y), pos(X2, Y2), NewMap) :-
    random_step(size(Width, Height), pos(X, Y), pos(X2, Y2)),
    map_insert(Map, pos(X2, Y2), empty, NewMap).

%! worm_passage(+Map: list, +Steps: number, +Size: term, +Pos: term, -NewMap: list) is det.
%
% Create a worm passage on Map.
%
% The worm passage will have Steps steps or empty cells one after another. It will start next
% to Pos (up, down, left, or right at random).
%
% @param Steps The amount of steps to create empty cells.
% @param Size The size of Map. A size/2 term with Width and Height.
% @param Pos The current position. A pos/2 term with X, Y coordinates.
worm_passage(Map, 0, _Size, _Pos, Map) :- !.
worm_passage(Map, Steps, Size, Pos, NewMap) :-
    %% Around Pos there is all blanks! just choose a random direction and go through it.
    map_all_around(Map, Pos, empty), !, 
    worm_step_ignore_empty(Map, Size, Pos, Pos2, Map2),
    Steps2 is Steps - 1,
    worm_passage(Map2, Steps2, Size, Pos2, NewMap).
worm_passage(Map, Steps, Size, Pos, NewMap) :-
    %% Choose a walled cell to do the next step.
    worm_step(Map, Size, Pos, Pos2, Map2),
    Steps2 is Steps - 1,
    worm_passage(Map2, Steps2, Size, Pos2, NewMap).

%! create_worm_passage(+Labyrinth: term, -NewLabyrinth: term) is det.
%
% Create a "worm passage". A passage that can be walked by the player.
create_worm_passage(labyrinth(map(Map), size(Width, Height), start(X,Y), Player),
                    labyrinth(map(NewMap), size(Width, Height), start(X,Y), Player)) :-
    map_insert(Map, pos(X, Y), empty, Map2),
    worm_passage(Map2, Width, size(Width, Height), pos(X, Y), NewMap).

%! create_worm_passage(+Labyrinth: term, -NewLabyrinth: term) is det.
%
% Create a "worm passage". A passage that can be walked by the player.
create_worm_passage2(labyrinth(map(Map), size(Width, Height), start(X,Y), Player),
                     labyrinth(map(NewMap), size(Width, Height), start(X,Y), Player)) :-
    map_insert(Map, pos(X, Y), empty, Map2),
    worm_passage2(Map2, size(Width, Height), Width, pos(X, Y), NewMap).

%! create_path(Map, Size, Pos, Direction, -Amount: number, -NewPos: term, -NewMap: list) is det.
%
% Create a path of empty cells from Start position following the given direction. Return the
% new starting and the generated map.
%
% @param Map The original map.
% @param Size The map size (a size/2 term with Width and Height).
% @param Start The position (pos/2 term) where to start the path.
% @param Direction The atom up, down, left, or right.
% @param Amount The number of empty cells to generate.
% @param NewPos The position (pos/2 term) where the path ended.
create_path(Map, _Size, Start, _Direction, 0, Start, Map) :- !.
create_path(Map, Size, Start, Direction, Amount, NewPos, NewMap) :-
    calc_step(Size, Start, Direction, Pos2), !, %% red cut
    map_insert(Map, Start, empty, Map2),
    Amount2 is Amount - 1,
    create_path(Map2, Size, Pos2, Direction, Amount2, NewPos, NewMap).
create_path(Map, _Size, Pos, _Direction, _Amount, Pos, Map).    

%! worm_passage2(+Map: list, +Size: term, +Steps: number, +Pos: term, -NewMap: list) is det.
%
% Another algorithm to create a worm passage. In this case, a direction and a number of cells is
% selected randomly. The number of cells is a random number between 2 to 10. Then, this number of
% cells are carved through the direction.
worm_passage2(Map, _Size, 0, _Pos, Map) :- !.
worm_passage2(Map, Size, Steps, Pos, NewMap) :-
    random_direction(Direction),
    random_between(2, 10, Amount),
    create_path(Map, Size, Pos, Direction, Amount, Pos2, Map2),
    Steps2 is Steps - 1,
    worm_passage2(Map2, Size, Steps2, Pos2, NewMap).
    
