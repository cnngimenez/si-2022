%% labyrinth2D.pl
%% Author: Gimenez, Christian.
%%
%% Copyright (C) 2024 Gimenez, Christian
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%
%% april 2024

:- module(labyrinth2D, [
              cell_type/2,
              new_empty_labyrinth/2,
              map_insert/4,
              map_at/3, map_at/4,
              map_all_around/3,
              map_find_around/4,              
              calc_step/4,
              write_labyrinth/1
          ]).
/** <module> labyrinth2D: 

@author Christian Gimenez
@license GPLv3
*/

:- use_module(library(random)).

cell_type(wall, 'x').
cell_type(empty, ' ').
cell_type(trap, 'T').

%! create_list(+N: number, +Element: term, -List: list) is det.
%
% Create a list of length N with Element elements only.
create_list(0, _Element, []) :- !.
create_list(N, Element, [Element|Rest]) :-
    N2 is N - 1,
    create_list(N2, Element, Rest), !.

%! create_matrix(+Width: number, +Height: number, +Element: term, -Matrix: list) is det.
%
% Create a list of lists (a 2D list or a matrix representation) of Width columns and Height rows.
% The matrix will be filled with Element.
create_matrix(Width, Height, Element, Matrix) :-
    create_list(Width, Element, Row),
    create_list(Height, Row, Matrix).

%! create_start(+Width, +Height, -X, -Y) is det.
%
% Create a starting position.
create_start(_Width, Height, 1, Y) :-
    random_between(1, Height, Y).

%! list_insert(+List: list, +X: number, +Element: term, -NewList: list) is det.
%
% Insert Element into List at position X.
list_insert([_|Rest], 1, Element, [Element|Rest]) :- !.
list_insert(List, X, Element, NewList) :-
    X2 is X - 1,
    length(Prefix, X2),
    append(Prefix, [_|Suffix], List),
    append(Prefix, [Element|Suffix], NewList).

%! map_insert(+Map: list, +Pos: term, +Element: term, -NewMap: list) is det.
%
% Insert Element into Map at the given position.
%
% @param Pos a pos/2 predicate with X,Y coordinates.
map_insert(Map, pos(X, Y), Element, NewMap) :-
    cell_type(Element, Char),
    nth1(Y, Map, Row),
    list_insert(Row, X, Char, NewRow),
    list_insert(Map, Y, NewRow, NewMap).

%! map_at(+Map: list, +Pos: term, ?Element: term) is det.
%
% What element is at the given position on the map?
%
% Fails if Pos is outside the map boundaries.
%
% @param Pos A pos/2 term.
% @see map_at/4
map_at(Map, pos(X, Y), Element) :-
    cell_type(Element, Char),
    nth1(Y, Map, Row),
    nth1(X, Row, Char).

%! map_at(+Map: list, +Pos: term, +Direction: atom, ?Element: term) is det.
%
% What element is at up/down/left/right of the given position on the map?
%
% Fail when it is trying to retrieve the element outside the map boundaries.
% For example, up or left of position pos(0,0).
%
% @param Pos A pos/2 term.
% @param Direction An atom: up, down, left, or right.
% @see map_at/3
map_at([Row|RMap], pos(X,Y), Direction, Element) :-
    length([Row|RMap], Height),
    length(Row, Width),
    calc_step(size(Width, Height), pos(X, Y), Direction, NewPos),
    map_at([Row|RMap], NewPos, Element).

%! map_all_around(+Map: list, +Position: term, ?Element: term) is det.
%
% Can Element be found around the given position on the map?
%
% True if Element is up, down, left, right at Position (not in Position itself). Ignore
% if one of the directions is outside of the map.
%
% Fails if one of the direction has a different element.
%
% @param Position A pos/2 term representing the center cell.
map_all_around(Map, Position, Element) :-
    %% ElementU/D/L/R = Element when the up/down/left/right of position is outside the map boundaries.
    %% This makes easy to ignore that position when that happens.
    (map_at(Map, Position, up, ElementU); ElementU = Element),
    (map_at(Map, Position, down, ElementD); ElementD = Element),
    (map_at(Map, Position, left, ElementL); ElementL = Element),
    (map_at(Map, Position, right, ElementR); ElementR = Element),
    %% Now, check if all ElementU/D/L/R are the same.
    Element = ElementU, Element = ElementD, Element = ElementL, Element = ElementR.

%! map_find_around(+Map: list, +Position: term, +Element: term, ?LstDirections: list)  is det.
%
% Return the directions where Element is found around the given Position.
%
% @param LstDirections A list of terms such as up, down, left, right where Element is present.
map_find_around(Map, Position, Element, LstDirections) :-
    findall(Dir, map_at(Map, Position, Dir, Element), LstDirections).

new_empty_labyrinth(Options, labyrinth(map(Map), size(Width, Height), start(X, Y), player(X, Y))) :-
    member(size(Width, Height), Options),
    cell_type(wall, Element),
    create_matrix(Width, Height, Element, Map),
    create_start(Width, Height, X, Y).

print_row(Row) :-
    atomic_list_concat(Row, Row2),
    writeln(Row2).

print_map([]).
print_map(Map) :-
    maplist(print_row, Map).

write_labyrinth(labyrinth(map(Map), _, _, _)) :-
    print_map(Map).

%! calc_step(+Size: term, +Pos: term, ?Direction: atom, ?NewPos: term) is det.
%
% Calculate the next step following the given direction. Fail when the next step
% is going to be outside the size (map).
%
% @param Size The size/2 term with Width and Height.
% @param Pos A pos/2 term with X, Y coordinates.
% @param Direction One of the following atoms: up, down, left, or right.
% @param NewPos A pos/2 term with X, Y coordinates.
calc_step(_Size, pos(X, Y), up, pos(X, Y2)) :- !,
    Y > 1,
    Y2 is Y - 1.
calc_step(size(_Width, Height), pos(X, Y), down, pos(X, Y2)) :- !,
    Y < Height,
    Y2 is Y + 1.
calc_step(_Size, pos(X, Y), left, pos(X2, Y)) :- !,
    X > 1,
    X2 is X - 1.
calc_step(size(Width, _Height), pos(X, Y), right, pos(X2, Y)) :-
    X < Width,
    X2 is X + 1.

